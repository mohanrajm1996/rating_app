# Create your views here.
from django.shortcuts import render
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout, get_user
from rest_framework import viewsets, permissions
from .models import App, User, TaskCompletion
from .serializer import AppSerializer, UserSerializer, TaskCompletionSerializer



class AppViewSet(viewsets.ModelViewSet):
    queryset = App.objects.all()
    serializer_class = AppSerializer
    permission_classes = [permissions.IsAuthenticated]

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]

class TaskCompletionViewSet(viewsets.ModelViewSet):
    queryset = TaskCompletion.objects.all()
    serializer_class = TaskCompletionSerializer
    permission_classes = [permissions.IsAuthenticated]

@login_required(login_url='login_view')
def home(request):
    user = request.user
    print(user)
    p_and_t = User.objects.get(username=request.user)
    return render(request, 'home.html', {'user': user, 'point': p_and_t},)

def login_view(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('home')
    return render(request, 'login.html')

def signout(request):
    logout(request)
    return redirect('home')

def signup_view(request):
    if request.method == 'POST':
        username = request.POST['username']
        password1 = request.POST['password1']
        password2 = request.POST['password2']
        profile_picture = request.FILES['profile_picture']
        if password1 == password2:
            user_a = User.objects.create_user(username=username, password=password1, profile_picture=profile_picture)
            user_a.save()
            return redirect('home')
        return redirect('signup_view')
    return render(request, 'signup.html')

@login_required
def apps_list(request):
    apps = App.objects.all()
    return render(request, 'apps_list.html', {'apps': apps})

@login_required
def app_detail(request, app_id):
    app = App.objects.get(id=app_id)
    return render(request, 'app_detail.html', {'app': app})

@login_required
def task_submit(request, app_id, app_name,points):
    point = User.objects.get(username=request.user)
    app = App.objects.get(name=app_name)
    if request.method == 'POST':
        screenshot = request.FILES['screenshot']
        task = TaskCompletion(user=point, app=app, screenshot=screenshot)
        task.save()

        point.points += points
        point.save()

        return redirect('home')
    return render(request, 'app_detail.html',)


